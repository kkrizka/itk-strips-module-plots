# ITk Strips Module Plots

A collection of scripts to make pretty plots of ITk Strips module testing
results.

## Installation
The recommended way to use the scripts is to install them in a virtual
environment. All dependencies are specified in the `setup.py` script.

```shell
python -m venv .venv
source .venv/bin/activate
pip install .
```

## Configuration
Local configuration can be created by making a `.modplot.yaml` file in your
current working directory. An template is available under
`modplot.yaml.example`.

The following options are available:
- `datadir`: Path to the directory containing the data files. Defaults to
             `data/`.

## Run Configurations
The operation of a script is controlled by a YAML file called run configuration.
Each script expects a different schema, however some effort has been made to
make them as standardized as possible. Example configuration files are provided
inside the `runconfigs` directory.

All paths in in the run configuration are relative `datadir` (see
Configuration).

Blocks are specific top-level fields.
### Common Top-Level Fields
The following top-level fields are common to all scripts.
- `title`: Title to put in each output plot.
- `name`: Prefix added to any output file name.
- `inputs`: A list of module test results files to use as input. See RC Inputs
            or IV Inputs for more details.

### Response Curve Inputs
A list of inputs for plotting the results of Response Curve tests. Each element
in the list is a dictionary with the following keys.

- `label`: Label for identifying the input.
- `xdata`: x hybrid result file (txt or json) located inside `datadir`.
- `ydata`: y hybrid result file (txt or json) located inside `datadir`.

### IV Scan Inputs
A list of inputs for plotting the results of IV scans. Each element in the list is a dictionary with the following keys.

- `label`: Label for identifying the input.
- `data`: IV scan output file (.json) located inside `datadir`.

## Scripts

The following is a list of available scripts. The example provided below are included to demonstrate how they work.

### `plot_compare.py`
Plots multiple RC curves on top of each other, along with a ratio (WOW!) with respect to the first entry. Separate output plots are made for each stream.

Example:
```shell
python plot_compare.py runconfigs/rc_highleakage_example.yaml
```

### `plot_ratios.py`
Plots a comparison of multiple RC curves as ratios. A single output is produced with all streams. Useful for making summary comparisons.

Example:
```shell
python plot_ratios.py runconfigs/rc_highleakage_example.yaml
```

### `plot_iv.py`
Plots a comparison of multiple IV curves. Two output files are provided with the following:
- AMAC IV curve showing the AMAC leakage current vs HVPS voltage.
- Power supply IV curve showing the HVPS current vs voltage.

Example:
```shell
python plot_iv.py runconfigs/iv_highleakage_example.yaml
```
