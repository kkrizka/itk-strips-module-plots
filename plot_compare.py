#!/usr/bin/env python

# %%
# %load_ext autoreload
# %autoreload 2

# %% Imporant imports
import sys
import argparse

import pandas as pd
import matplotlib.pyplot as plt

import kkplot

from modplot import data
from modplot import tools
from modplot import config

# %% Prepare configuration
xmin=-1
xmax=1280

if 'ipykernel' in sys.modules: # running in a notebook
    runcfgpaths=['runconfigs/data_20221005.yaml']
else:
    parser = argparse.ArgumentParser('Plot multiple RC scans.')
    parser.add_argument('runcfgpath',nargs='+')
    parser.add_argument('--xmin',default=xmin,type=int,help='Minimum for x-axis.')
    parser.add_argument('--xmax',default=xmax,type=int,help='Maximum for x-axis.')
    args=parser.parse_args()

    runcfgpaths=args.runcfgpath
    xmin=args.xmin
    xmax=args.xmax

runcfg = tools.load_runconfigs(runcfgpaths)

# %% Global plotting options
name = runcfg.get('name','unknown')
title = runcfg.get('title',None)

# %%
def plot_ratio(df, df_ref, label, ax):
    ax[0].plot(df['chan'],df['innse'],'.',label=label)

    dfm=df_ref.merge(df, on=['chan','stream'])
    dfm['innse_ratio']=dfm.innse_y/dfm.innse_x

    ax[1].plot(dfm['chan'],dfm['innse_ratio'],'.')

def style_ratio(ax, title, xmin, xmax):
    ax[0].grid(True, axis='x')
    ax[1].grid(True, axis='x')

    ax[0].set_title(title)

    ax[0].set_ylim(0,1500)
    #ax[0].set_ylim(0,10000)

    # ratio
    ax[1].set_xlabel('Channel')

    ax[1].set_xlim(xmin,xmax)

    kkplot.xticks(128,16,ax[1])
    #kkplot.xticks(10,1,ax[1])

    ax[1].set_ylim(0.9,1.1)

def plot_histogram(df, label, ax):
    ax.hist(df['innse'],bins=100,range=(600,1000),label=label,density=True,histtype='step')

# %% Load the ratio histograms
ratio = runcfg.get('ratio',{})
ratio_idx = ratio.get('index',0)
ratio_input = runcfg['inputs'][ratio_idx]
ratio_ylabel = ratio_input['label']

ratio_ymin = ratio.get('ymin',0.75)
ratio_ymax = ratio.get('ymax',1.25)

dfx_ref=None
dfy_ref=None

if 'xdata' in ratio_input:
    dfx_ref = data.load_rc(f'{config.datadir}/{ratio_input["xdata"]}')

if 'ydata' in ratio_input:
    dfy_ref=data.load_rc(f'{config.datadir}/{ratio_input["ydata"]}')

# %%
plots=[kkplot.subplot_ratio() for i in range(4)]
fig, ax = zip(*plots)

for input in runcfg['inputs']:
    # Plot the x hybrids
    if 'xdata' in input:
        dfx=data.load_rc(f'{config.datadir}/{input["xdata"]}')

        dfx0=dfx[dfx.stream==0]
        dfx1=dfx[dfx.stream==1]

        if dfx_ref is None:
            dfx_ref=dfx

        plot_ratio(dfx0, dfx_ref, input['label'], ax[0])
        plot_ratio(dfx1, dfx_ref, input['label'], ax[1])

    # Plot the y hybrids
    if 'ydata' in input:
        dfy=data.load_rc(f'{config.datadir}/{input["ydata"]}')

        dfy0=dfy[dfy.stream==0]
        dfy1=dfy[dfy.stream==1]

        if dfy_ref is None:
            dfy_ref=dfy

        plot_ratio(dfy0, dfy_ref, input['label'], ax[2])
        plot_ratio(dfy1, dfy_ref, input['label'], ax[3])

# style the plots
style_ratio(ax[0],'hybrid x, stream 0', xmin=xmin, xmax=xmax)
style_ratio(ax[1],'hybrid x, stream 1', xmin=xmin, xmax=xmax)
style_ratio(ax[2],'hybrid y, stream 0', xmin=xmin, xmax=xmax)
style_ratio(ax[3],'hybrid y, stream 1', xmin=xmin, xmax=xmax)

for i in range(4):
    ax[i][0].set_ylabel('Input Noise')
    ax[i][0].legend(ncol=2)

    ax[i][1].set_ylabel(f'1/{ratio_ylabel}')
    ax[i][1].set_ylim(ratio_ymin,ratio_ymax)

    fig[i].suptitle(title)

fig[0].savefig(f'{name}_compare_x0.png')
fig[1].savefig(f'{name}_compare_x1.png')
fig[2].savefig(f'{name}_compare_y0.png')
fig[3].savefig(f'{name}_compare_y1.png')

# %% Plot a histogram of noises

# %%
plots=[plt.subplots() for i in range(4)]
fig, ax = zip(*plots)

for input in runcfg['inputs']:
    # Plot the x hybrids
    if 'xdata' in input:
        dfx=data.load_rc(f'{config.datadir}/{input["xdata"]}')

        dfx0=dfx[dfx.stream==0]
        dfx1=dfx[dfx.stream==1]

        plot_histogram(dfx0, input['label'], ax[0])
        plot_histogram(dfx1, input['label'], ax[1])

    # Plot the y hybrids
    if 'ydata' in input:
        dfy=data.load_rc(f'{config.datadir}/{input["ydata"]}')

        dfy0=dfy[dfy.stream==0]
        dfy1=dfy[dfy.stream==1]

        plot_histogram(dfy0, input['label'], ax[2])
        plot_histogram(dfy1, input['label'], ax[3])

# # style the plots
# style_ratio(ax[0],'hybrid x, stream 0', xmin=xmin, xmax=xmax)
# style_ratio(ax[1],'hybrid x, stream 1', xmin=xmin, xmax=xmax)
# style_ratio(ax[2],'hybrid y, stream 0', xmin=xmin, xmax=xmax)
# style_ratio(ax[3],'hybrid y, stream 1', xmin=xmin, xmax=xmax)

for i in range(4):
    ax[i].set_xlabel('Input Noise')
    ax[i].set_xlim(600,1000)

    kkplot.xticks(100,10, ax[i])
    ax[i].legend()

    fig[i].suptitle(title)

fig[0].savefig(f'{name}_comparehist_x0.png')
fig[1].savefig(f'{name}_comparehist_x1.png')
fig[2].savefig(f'{name}_comparehist_y0.png')
fig[3].savefig(f'{name}_comparehist_y1.png')

