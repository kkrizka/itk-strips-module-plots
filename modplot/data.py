"""
Functions for loading data.
"""

import pandas as pd
import json

def load_rc(path):
    """
    Load the RC and split it into streams using the corresponding file
    type function.

    Adds the following columns:
        stream: stream number
        chan: channel within the stream
    """
    if path.endswith('.txt'):
        return txt_load_rc(path)

    if path.endswith('.json'):
        return json_load_rc(path)

    raise Exception('Unknown RC file type.')
    return None

def txt_load_rc(path):
    """
    Load the RC txt data file and split it into streams.

    Should be used through `load_rc` for extra processing.
    """
    df=pd.read_csv(path, sep='\t')
    df['stream']=(df['#chan']>=1280).astype(int)
    df['chan']=df['#chan']-1280*df['stream']

    return df

def json_load_rc(path):
    """
    Load the JSON txt data file and split it into streams.

    Should be used through `load_rc` for extra processing.
    """
    with open(path, "r") as f:
        data=json.load(f)
    # Just load everything into a dataframe. We'll parse it later.
    df=pd.json_normalize(data)

    # Drop aggregate columns.
    agg_cols=list(filter(lambda col: '_mean' in col or '_rms' in col, df.columns))
    df.drop(columns=agg_cols, inplace=True)

    # Split into streams
    other_cols=list(filter(lambda col: not col.endswith('away') and not col.endswith('under'), df.columns))
    away_cols =list(filter(lambda col: col.endswith('away'), df.columns))
    under_cols=list(filter(lambda col: col.endswith('under'), df.columns))

    df0=df[other_cols+under_cols].copy()
    df1=df[other_cols+away_cols ].copy()

    df0['stream']=0
    df1['stream']=1

    df0.rename(columns=lambda col: col.replace('_under',''), inplace=True)
    df1.rename(columns=lambda col: col.replace('_away',''), inplace=True)

    df=pd.concat([df0,df1])

    # Get rid of results prefix
    # Also keep a list of results columns (after rename) for futher operations
    df.drop(columns='results.rc_fit', inplace=True) # Not use what this is
    df.drop(columns='results.outnse', inplace=True) # Not use what this is

    results_cols=filter(lambda col: col.startswith('results.'), df.columns)
    results_cols=list(map(lambda col: col.replace('results.',''), results_cols))

    df.rename(columns=lambda col: col.replace('results.',''), inplace=True)

    # Explode the lists of data
    df=df.explode(results_cols, ignore_index=True).explode(results_cols, ignore_index=True)

    # Add the channel number. Here we assume that everything is still ordered by channel
    # as part of the explode operations above and that the streams have equal numbers of
    # channels.
    nChan=len(df.index)//2
    df['chan']=df.index%nChan

    return df.reset_index()

def load_iv(path):
    """
    Load the IV JSON data file.
    """
    itsdaqdata=json.load(open(path))
    modulesn=itsdaqdata['component']

    # Extract results keys that are columns
    data={}
    for key,value in itsdaqdata['results'].items():
        if isinstance(value, list):
            data[key]=value
    for key,value in itsdaqdata['properties'].items():
        if isinstance(value, list):
            data[key]=value

    # Build the dataframe
    df=pd.DataFrame(data)
    df['component']=modulesn
    df['date']=itsdaqdata['date']

    return df