#!/usr/bin/env python

# %%
# %load_ext autoreload
# %autoreload 2

# %% Imporant imports
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from math import *

import kkplot

from modplot import data
from modplot import tools
from modplot import config

# %% Prepare configuration
if 'ipykernel' in sys.modules: # running in a notebook
    runcfgpaths=['runconfigs/iv_highleakage.yaml']
else:
    if len(sys.argv)<2:
        print('usage: {} runconfig.yaml [runconfig.yaml]'.format(sys.argv[0]))
        sys.exit(1)
    runcfgpaths=sys.argv[1:]

runcfg = tools.load_runconfigs(runcfgpaths)

# %% Global plotting options
name = runcfg.get('name','unknown')
title = runcfg.get('title',None)

# %% Load the input data
dfs=map(lambda entry: data.load_iv(config.datadir+'/'+entry['data']), runcfg['inputs'])
df=pd.concat(dfs, keys=range(len(runcfg['inputs'])))

# %%
fig,ax=plt.subplots()

for date, sdf in df.groupby(level=0):
    label=runcfg['inputs'][date].get('label',date)
    marker=runcfg['inputs'][date].get('marker',None)
    ax.plot(sdf['VOLTAGE'],sdf['CURRENT'],'-',label=label,marker=marker)
#ax.legend()

ax.set_title(title)

ax.set_xlabel('Bias Voltage [V]')
ax.set_ylabel('Current [nA]')

ax.set_xlim(-510,10)
# ax.set_ylim(0,2000)
# ax.set_xlim(-710,10)
ax.set_ylim(0,500)

kkplot.xticks(100,10,ax=ax)
kkplot.yticks(200,50,ax=ax)

fig.tight_layout()

fig.savefig(f'{name}_iv.png')
fig.show()

# %%
fig,ax=plt.subplots()

ax.plot([0,-500],[0,50],'--k',label='1μA/10V')
for date, sdf in df.groupby(level=0):
    label=runcfg['inputs'][date].get('label',date)
    marker=runcfg['inputs'][date].get('marker',None)
    ax.plot(sdf['VOLTAGE'],np.abs(sdf['PS_CURRENT']),'-',label=label,marker=marker)

#ax.legend()

fig.suptitle(title)

ax.set_xlabel('Bias Voltage [V]')
ax.set_ylabel('PS Current [μA]')

ax.set_xlim(-510,10)
ax.set_ylim(0,60)

kkplot.xticks(100,10,ax=ax)
kkplot.yticks(10,2,ax=ax)

fig.tight_layout()

fig.savefig(f'{name}_psiv.png')
fig.show()
# %%
