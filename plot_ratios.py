#!/usr/bin/env python

# %%
# %load_ext autoreload
# %autoreload 2

# %% Imporant imports
import sys
import pandas as pd
import matplotlib.pyplot as plt

from math import *

import kkplot

from modplot import data
from modplot import tools
from modplot import config

# %% Prepare configuration
if 'ipykernel' in sys.modules: # running in a notebook
    runcfgpaths=['runconfigs/data_20221005.yaml']
else:
    if len(sys.argv)<2:
        print('usage: {} runconfig.yaml [runconfig.yaml]'.format(sys.argv[0]))
        sys.exit(1)
    runcfgpaths=sys.argv[1:]

runcfg = tools.load_runconfigs(runcfgpaths)

# %% Global plotting options
name = runcfg.get('name','unknown')
title = runcfg.get('title',None)

# %%
def plot_ratio(df, df_ref, label, ax):
    dfm=df_ref.merge(df, on=['chan','stream'])
    dfm['innse_ratio']=dfm.innse_y/dfm.innse_x

    ax.plot(dfm['chan'],dfm['innse_ratio'],'.', label=label)


# %%
fig,ax=plt.subplots(nrows=4, sharex=True, sharey=True, figsize=(6.4,8), gridspec_kw={'hspace':0.3})

dfx_ref=None
dfy_ref=None

for input in runcfg['inputs']:
    if 'xdata' in input:
        dfx=data.load_rc(f'{config.datadir}/{input["xdata"]}')

        dfx0=dfx[dfx.stream==0]
        dfx1=dfx[dfx.stream==1]

        if dfx_ref is None:
            dfx_ref=dfx

        plot_ratio(dfx0, dfx_ref, input['label'], ax[0])
        plot_ratio(dfx1, dfx_ref, input['label'], ax[1])

    if 'ydata' in input:
        dfy=data.load_rc(f'{config.datadir}/{input["ydata"]}')

        dfy0=dfy[dfy.stream==0]
        dfy1=dfy[dfy.stream==1]

        if dfy_ref is None:
            dfy_ref=dfy

        plot_ratio(dfy0, dfy_ref, input['label'], ax[2])
        plot_ratio(dfy1, dfy_ref, input['label'], ax[3])

# style the plots
ax[0].set_title('hybrid x, stream 0')
ax[1].set_title('hybrid x, stream 1')
ax[2].set_title('hybrid y, stream 0')
ax[3].set_title('hybrid y, stream 1')

# ratio
ax[3].set_xlabel('Channel')
ax[3].set_xlim(-1,1280)
kkplot.xticks(128,16,ax[3])

ax[1].legend(ncol=ceil(len(runcfg['inputs'])/2))

for myax in ax:
    myax.set_ylabel('Ratio')

ax[3].set_ylim(runcfg.get('rmin',0.75),runcfg.get('rmax',1.25))

fig.suptitle(f'{title}')

fig.savefig(f'{name}_ratio.png')

# %%
